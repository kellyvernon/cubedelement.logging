﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExceptionOnly
{
    public class AndDebugging : AndLoggingBase
    {
        private string _fileContents;
        private Exception _exception;

        public AndDebugging() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());

            Logger.Debug(_exception);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void ItShouldWriteDebugMessageToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteLogTypeDebugToFile()
        {
            Assert.Contains(_fileContents, "DEBUG");
        }
    }
}