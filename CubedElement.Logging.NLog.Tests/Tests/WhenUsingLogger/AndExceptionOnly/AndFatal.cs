﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExceptionOnly
{
    public class AndFatal : AndLoggingBase
    {
        private string _fileContents;
        private Exception _exception;

        public AndFatal() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());
            
            Logger.Fatal(_exception);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void ItShouldWriteFatalMessageToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteLogTypeFatalToFile()
        {
            Assert.Contains(_fileContents, "FATAL");
        }
    }
}