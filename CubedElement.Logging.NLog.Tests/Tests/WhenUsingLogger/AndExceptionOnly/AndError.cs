﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExceptionOnly
{

    public class AndError : AndLoggingBase
    {
        private string _fileContents;
        private Exception _exception;

        public AndError() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());

            Logger.Error(_exception);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void ItShouldWriteErrorMessageToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteLogTypeErrorToFile()
        {
            Assert.Contains(_fileContents, "ERROR");
        }
    }
}