﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExceptionOnly
{
    public class AndInfo : AndLoggingBase
    {
        private string _fileContents;
        private Exception _exception;

        public AndInfo() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());

            Logger.Info(_exception);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void ItShouldWriteInfoMessageToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteLogTypeInfoToFile()
        {
            Assert.Contains(_fileContents, "INFO");
        }
    }
}