﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExceptionOnly
{
    public class AndWarning : AndLoggingBase
    {
        private string _fileContents;
        private Exception _exception;

        public AndWarning() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());

            Logger.Warn(_exception);

            _fileContents = GetFileContents(FilePathAndName);
        }
        
        [Fact]
        public void ItShouldWriteWarnMessageToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteLogTypeWarnToFile()
        {
            Assert.Contains(_fileContents, "WARN");
        }
    }
}