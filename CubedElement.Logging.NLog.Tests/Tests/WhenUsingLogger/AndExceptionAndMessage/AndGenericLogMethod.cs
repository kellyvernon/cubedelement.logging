﻿using System;
using System.Collections.Generic;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExceptionAndMessage
{
    public class AndGenericLogMethod : AndLoggingBase
    {
        private string _fileContents;
        private List<LogType> _allLogType;
        private string _exceptionMessage;

        public AndGenericLogMethod() : base()
        {
            base.FixtureSetUp();

            _allLogType = new List<LogType>
            {
                LogType.Debug,
                LogType.Error,
                LogType.Fatal,
                LogType.Info,
                LogType.Trace,
                LogType.Warn
            };

            _exceptionMessage = string.Format("problem getting contents writing to: {0}", FilePathAndName);
        }

        [Fact]
        public void AndExceptionNull()
        {
            foreach (var logType in _allLogType)
            {
                var name = Enum.GetName(typeof(LogType), logType);
                if (name == null)
                {
                    Assert.True(false, "why is this null?");
                }

                var message = Guid.NewGuid().ToString();

                Logger.Log(logType, message, null);

                _fileContents = GetFileContents(FilePathAndName);

                Assert.Contains(_fileContents, message);
                Assert.Contains(_fileContents, name.ToUpper());
            }
        }

        [Fact]
        public void ItShouldWriteMessageToFile()
        {
            foreach (var logType in _allLogType)
            {
                var name = Enum.GetName(typeof(LogType), logType);
                if (name == null)
                {
                    Assert.True(false, "why is this null?");
                }

                var exception = new Exception(Guid.NewGuid().ToString());
                var message = Guid.NewGuid().ToString();

                Logger.Log(logType, message, exception);

                _fileContents = GetFileContents(FilePathAndName);

                Assert.Contains(_fileContents, exception.ToString());
                Assert.Contains(_fileContents, message);
                Assert.Contains(_fileContents, name.ToUpper());
            }
        }
    }
}