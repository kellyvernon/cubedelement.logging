﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExceptionAndMessage
{
    public class AndDebugging : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        private Exception _exception;

        public AndDebugging() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());

            _message = Guid.NewGuid().ToString();

            Logger.Debug(_message, _exception);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void ItShouldWriteDebugExceptionToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteDebugMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeDebugToFile()
        {
            Assert.Contains(_fileContents, "DEBUG");
        }
    }
}