﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExceptionAndMessage
{
    public class AndTracing : AndLoggingBase
    {
        private string _fileContents;
        private Exception _exception;
        private string _message;

        public AndTracing() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());

            _message = Guid.NewGuid().ToString();

            Logger.Trace(_message, _exception);

            _fileContents = GetFileContents(FilePathAndName);
        }
        
        [Fact]
        public void ItShouldWriteFatalExceptionToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteTraceMessageToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteLogTypeTraceToFile()
        {
            Assert.Contains(_fileContents, "TRACE");
        }
    }
}