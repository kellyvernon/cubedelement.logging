﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;
namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger
{
    
    public class AndInitializing
    {
        private ILogger _logger;

        [Fact]
        public void AndNoClassNameItShouldNotFail()
        {
            Record.Exception(() =>
            {
                _logger = new Logger();
            });
        }

        [Fact]
        public void AndClassNameItShouldNotFail()
        {
            Record.Exception(() =>
            {
                _logger = new Logger(Guid.NewGuid().ToString());
            });
        }
    }
}