﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndMessageOnly
{
    public class AndError : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndError() : base()
        {
            base.FixtureSetUp();

            _message = Guid.NewGuid().ToString();
            Logger.Error(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void ItShouldWriteErrorMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeErrorToFile()
        {
            Assert.Contains(_fileContents, "ERROR");
        }
    }
}