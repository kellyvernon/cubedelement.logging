﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndMessageOnly
{
    public class AndInfo : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndInfo()
        {
            base.FixtureSetUp();

            _message = Guid.NewGuid().ToString();
            
            Logger.Info(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void ItShouldWriteInfoMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeInfoToFile()
        {
            Assert.Contains(_fileContents, "INFO");
        }
    }
}