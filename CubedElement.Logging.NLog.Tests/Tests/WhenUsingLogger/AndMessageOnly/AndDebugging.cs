﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndMessageOnly
{
    public class AndDebugging : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndDebugging() : base()
        {
            base.FixtureSetUp();

            _message = Guid.NewGuid().ToString();
            Logger.Debug(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void ItShouldWriteDebugMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeDebugToFile()
        {
            Assert.Contains(_fileContents, "DEBUG");
        }
    }
}