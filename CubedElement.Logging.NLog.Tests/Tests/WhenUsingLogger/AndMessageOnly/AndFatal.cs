﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndMessageOnly
{
    public class AndFatal : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndFatal() : base()
        {
            base.FixtureSetUp();

            _message = Guid.NewGuid().ToString();
            Logger.Fatal(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void ItShouldWriteFatalMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeFatalToFile()
        {
            Assert.Contains(_fileContents, "FATAL");
        }
    }
}