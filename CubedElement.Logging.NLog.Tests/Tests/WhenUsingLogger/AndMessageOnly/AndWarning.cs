﻿using System;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndMessageOnly
{
    public class AndWarning : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndWarning() : base()
        {
            base.FixtureSetUp();

            _message = Guid.NewGuid().ToString();
            
            Logger.Warn(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }
        
        [Fact]
        public void ItShouldWriteWarnMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeWarnToFile()
        {
            Assert.Contains(_fileContents, "WARN");
        }
    }
}