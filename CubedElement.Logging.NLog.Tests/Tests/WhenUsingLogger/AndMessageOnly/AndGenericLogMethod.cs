﻿using System;
using System.Collections.Generic;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndMessageOnly
{
    public class AndGenericLogMethod : AndLoggingBase
    {
        private string _fileContents;
        private List<LogType> _allLogType;
        
        public AndGenericLogMethod() : base()
        {
            base.FixtureSetUp();

            _allLogType = new List<LogType>
            {
                LogType.Debug,
                LogType.Error,
                LogType.Fatal,
                LogType.Info,
                LogType.Trace,
                LogType.Warn
            };
        }

        [Fact]
        public void ItShouldWriteMessageToFile()
        {
            var exceptionMessage = string.Format("problem getting contents writing to: {0}", FilePathAndName);
            
            foreach (var logType in _allLogType)
            {
                var name = Enum.GetName(typeof(LogType), logType);
                if (name == null)
                {
                    Assert.True(false, "why is this null?");
                }

                var message = Guid.NewGuid().ToString();
                Logger.Log(logType, message);
                _fileContents = GetFileContents(FilePathAndName);

                Assert.Contains(_fileContents, message);
                Assert.Contains(_fileContents, name.ToUpper());
            }
        }
    }
}