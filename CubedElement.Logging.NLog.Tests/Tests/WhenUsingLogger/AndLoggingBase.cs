﻿using System;
using System.IO;
using System.Reflection;
using CubedElement.Logging.Contracts;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger
{
    public class AndLoggingBase : IDisposable
    {
        protected string FilePathAndName;
        
        protected ILogger Logger;
        
        public virtual void FixtureSetUp()
        {
            FilePathAndName = GetLogPath("log-test.txt");
                        
            Logger = new Logger();
        }

        protected static string GetFileContents(string filePathAndName)
        {
            if (string.IsNullOrEmpty(filePathAndName))
            {
                return string.Empty;
            }

            return !File.Exists(filePathAndName) 
                ? string.Empty 
                : File.ReadAllText(filePathAndName);
        }

        private string GetLogPath(string fileName)
        {
            var assembly = Assembly.GetEntryAssembly();

            var replace = assembly.Location.Replace($"{assembly.GetName().Name}.dll", string.Empty);

            return Path.Combine(replace, fileName);
        }

        public void Dispose()
        {
            if (File.Exists(FilePathAndName))
            {
                File.Delete(FilePathAndName);
            }
        }
    }
}