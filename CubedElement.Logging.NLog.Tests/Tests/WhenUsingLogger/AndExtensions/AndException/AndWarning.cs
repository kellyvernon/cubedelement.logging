﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndException
{
    public class AndWarning : AndLoggingBase
    {
        private string _fileContents;
        private Exception _exception;

        public AndWarning() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());

            Logger.WriteWarn(_exception);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteWarn(Guid.NewGuid().ToString(), new Exception()));
        }

        [Fact]
        public void ItShouldWriteFatalExceptionToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteLogTypeWarnToFile()
        {
            Assert.Contains(_fileContents, "WARN");
        }
    }
}