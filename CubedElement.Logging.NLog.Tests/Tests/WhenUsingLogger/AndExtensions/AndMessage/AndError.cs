﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndMessage
{
    public class AndError : AndLoggingBase
    {
        private string _fileContents;
        private string _message;

        
        public AndError() : base()
        {
            base.FixtureSetUp();

            _message = Guid.NewGuid().ToString();

            Logger.WriteError(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteError(_message));
        }

        [Fact]
        public void ItShouldWriteErrorMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeErrorToFile()
        {
            Assert.Contains(_fileContents, "ERROR");
        }
    }
}