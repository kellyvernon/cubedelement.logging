﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndMessage
{
    public class AndDebugging : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndDebugging() : base()
        {
            _message = Guid.NewGuid().ToString();

            Logger.WriteDebug(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteDebug(_message));
        }

        [Fact]
        public void ItShouldWriteDebugMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeDebugToFile()
        {
            Assert.Contains(_fileContents, "DEBUG");
        }
    }
}