﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndMessage
{
    public class AndFatal : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndFatal() : base()
        {
            _message = Guid.NewGuid().ToString();

            Logger.WriteFatal(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteFatal(_message));
        }

        [Fact]
        public void ItShouldWriteFatalMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeFatalToFile()
        {
            Assert.Contains(_fileContents, "FATAL");
        }
    }
}