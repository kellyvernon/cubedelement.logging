﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndMessage
{
    public class AndWarning : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndWarning()
        {
            _message = Guid.NewGuid().ToString();

            Logger.WriteWarn(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteWarn(Guid.NewGuid().ToString(), new Exception()));
        }

        [Fact]
        public void ItShouldWriteWarnMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeWarnToFile()
        {
            Assert.Contains(_fileContents, "WARN");
        }
    }
}