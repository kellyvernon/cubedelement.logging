﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndMessage
{
    public class AndTracing : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndTracing() : base()
        {
            base.FixtureSetUp();

            _message = Guid.NewGuid().ToString();

            Logger.WriteTrace(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteTrace(_message));
        }

        [Fact]
        public void ItShouldWriteTraceMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeTraceToFile()
        {
            Assert.Contains(_fileContents, "TRACE");
        }
    }
}