﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndMessage
{
    public class AndInfo : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        
        public AndInfo()
        {
            _message = Guid.NewGuid().ToString();

            Logger.WriteInfo(_message);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteInfo(_message));
        }

        [Fact]
        public void ItShouldWriteInfoMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeInfoToFile()
        {
            Assert.Contains(_fileContents, "INFO");
        }
    }
}