﻿using System;
using System.Collections.Generic;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndExceptionAndMessage
{
    public class AndGenericLogMethod : AndLoggingBase
    {
        private string _fileContents;
        private List<LogType> _allLogType;

        
        public AndGenericLogMethod() : base()
        {
            base.FixtureSetUp();

            _allLogType = new List<LogType>
            {
                LogType.Debug,
                LogType.Error,
                LogType.Fatal,
                LogType.Info,
                LogType.Trace,
                LogType.Warn
            };
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteLog(LogType.Debug, Guid.NewGuid().ToString(), new Exception()));
        }

        [Fact]
        public void AndLoggerNotNullItShouldWriteMessageToFile()
        {
            var exceptionMessage = string.Format("problem getting contents writing to: {0}", FilePathAndName);
            
            foreach (var logType in _allLogType)
            {
                var name = Enum.GetName(typeof(LogType), logType);
                if (name == null)
                {
                    Assert.True(false, "why is this null?");
                }

                var exception = new Exception(Guid.NewGuid().ToString());
                var message = Guid.NewGuid().ToString();

                Logger.WriteLog(logType, message, exception);

                _fileContents = GetFileContents(FilePathAndName);

                Assert.Contains(_fileContents, exception.ToString());
                Assert.Contains(_fileContents, message);
                Assert.Contains(_fileContents, name.ToUpper());
            }
        }
    }
}