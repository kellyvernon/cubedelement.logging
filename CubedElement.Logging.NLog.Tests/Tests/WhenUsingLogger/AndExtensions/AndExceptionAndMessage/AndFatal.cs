﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndExceptionAndMessage
{
    public class AndFatal : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        private Exception _exception;

        
        public AndFatal() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());

            _message = Guid.NewGuid().ToString();

            Logger.WriteFatal(_message, _exception);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteFatal(Guid.NewGuid().ToString(), new Exception()));
        }

        [Fact]
        public void ItShouldWriteFatalExceptionToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteFatalMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeFatalToFile()
        {
            Assert.Contains(_fileContents, "FATAL");
        }
    }
}