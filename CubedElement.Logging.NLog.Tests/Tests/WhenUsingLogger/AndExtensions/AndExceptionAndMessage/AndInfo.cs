﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndExceptionAndMessage
{
    public class AndInfo : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        private Exception _exception;

        
        public AndInfo() : base()
        {
            _exception = new Exception(Guid.NewGuid().ToString());
            _message = Guid.NewGuid().ToString();

            Logger.WriteInfo(_message, _exception);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteInfo(Guid.NewGuid().ToString(), new Exception()));
        }

        [Fact]
        public void ItShouldWriteFatalExceptionToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteInfoMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeInfoToFile()
        {
            Assert.Contains(_fileContents, "INFO");
        }
    }
}