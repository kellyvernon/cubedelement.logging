﻿using System;
using CubedElement.Logging.Contracts;
using Xunit;

namespace CubedElement.Logging.NLog.Tests.Tests.WhenUsingLogger.AndExtensions.AndExceptionAndMessage
{
    public class AndError : AndLoggingBase
    {
        private string _fileContents;
        private string _message;
        private Exception _exception;

        public AndError() : base()
        {
            base.FixtureSetUp();

            _exception = new Exception(Guid.NewGuid().ToString());

            _message = Guid.NewGuid().ToString();

            Logger.WriteError(_message, _exception);

            _fileContents = GetFileContents(FilePathAndName);
        }

        [Fact]
        public void AndLoggerNullItShouldNotThrowException()
        {
            Logger = null;

            Record.Exception(() => Logger.WriteError(Guid.NewGuid().ToString(), new Exception()));
        }

        [Fact]
        public void ItShouldWriteErrorExceptionToFile()
        {
            Assert.Contains(_fileContents, _exception.ToString());
        }

        [Fact]
        public void ItShouldWriteErrorMessageToFile()
        {
            Assert.Contains(_fileContents, _message);
        }

        [Fact]
        public void ItShouldWriteLogTypeErrorToFile()
        {
            Assert.Contains(_fileContents, "ERROR");
        }
    }
}