﻿using System;
using CubedElement.Logging.Contracts;
using NLog;

namespace CubedElement.Logging.NLog
{
    /// <summary>
    /// basic wrapper for NLOG2's logger
    /// </summary>
    public class Logger : CubedElement.Logging.Contracts.ILogger
    {
        private readonly global::NLog.Logger _loggerMgr;

        public Logger(string className = null)
        {
            if (string.IsNullOrEmpty(className))
            {
                className = GetType().FullName;
            }

            _loggerMgr = LogManager.GetLogger(className);
        }

        public void Trace(string message)
        {
            Log(LogType.Trace, message);
        }

        public void Trace(Exception exception)
        {
            Log(LogType.Trace, exception);
        }

        public void Trace(string message, Exception exception)
        {
            Log(LogType.Trace, message, exception);
        }

        public void Info(string message)
        {
            Log(LogType.Info, message);
        }

        public void Info(Exception exception)
        {
            Log(LogType.Info, exception);
        }

        public void Info(string message, Exception exception)
        {
            Log(LogType.Info, message, exception);
        }

        public void Warn(string message)
        {
            Log(LogType.Warn, message);
        }

        public void Warn(Exception exception)
        {
            Log(LogType.Warn, exception);
        }

        public void Warn(string message, Exception exception)
        {
            Log(LogType.Warn, message, exception);
        }

        public void Debug(string message)
        {
            Log(LogType.Debug, message);
        }

        public void Debug(Exception exception)
        {
            Log(LogType.Debug, exception);
        }

        public void Debug(string message, Exception exception)
        {
            Log(LogType.Debug, message, exception);
        }

        public void Error(string message)
        {
            Log(LogType.Error, message);
        }

        public void Error(Exception exception)
        {
            Log(LogType.Error, exception);
        }

        public void Error(string message, Exception exception)
        {
            Log(LogType.Error, message, exception);
        }

        public void Fatal(string message)
        {
            Log(LogType.Fatal, message);
        }

        public void Fatal(Exception exception)
        {
            Log(LogType.Fatal, exception);
        }

        public void Fatal(string message, Exception exception)
        {
            Log(LogType.Fatal, message, exception);
        }

        public void Log(LogType logType, string message)
        {
            _loggerMgr.Log(ConvertLogType(logType), message);
        }

        public void Log(LogType logType, Exception exception)
        {
            _loggerMgr.Log(ConvertLogType(logType), exception);
        }

        public void Log(LogType logType, string message, Exception exception)
        {
            _loggerMgr.Log(ConvertLogType(logType), exception, message);
        }

        private static LogLevel ConvertLogType(LogType logType)
        {
            LogLevel loggingType;

            switch (logType)
            {
                case LogType.Debug:
                    loggingType = LogLevel.Debug;
                    break;
                case LogType.Error:
                    loggingType = LogLevel.Error;
                    break;
                case LogType.Fatal:
                    loggingType = LogLevel.Fatal;
                    break;
                case LogType.Info:
                    loggingType = LogLevel.Info;
                    break;
                case LogType.Trace:
                    loggingType = LogLevel.Trace;
                    break;
                default:
                    loggingType = LogLevel.Warn;
                    break;
            }

            return loggingType;
        }
    }
}
