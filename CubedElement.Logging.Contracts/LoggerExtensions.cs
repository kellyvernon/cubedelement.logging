﻿using System;

namespace CubedElement.Logging.Contracts
{
    /// <summary>
    /// A series of extensions that allows you to do all the logging as before, but you don't need to worry about if the logger is null
    /// </summary>
    public static class LoggerExtensions
    {
        /// <summary>
        /// You choose the <see cref="LogType"/> through this 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="logType"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void WriteLog(this ILogger logger, LogType logType, string message, Exception exception)
        {
            logger?.Log(logType, message, exception);
        }

        /// <summary>
        /// You choose the <see cref="LogType"/> through this 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="logType"></param>
        /// <param name="exception"></param>
        public static void WriteLog(this ILogger logger, LogType logType, Exception exception)
        {
            logger?.Log(logType, exception);
        }

        /// <summary>
        /// You choose the <see cref="LogType"/> through this 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="logType"></param>
        /// <param name="message"></param>
        public static void WriteLog(this ILogger logger, LogType logType, string message)
        {
            logger?.Log(logType, message);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Debug"/> statement with message and exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void WriteDebug(this ILogger logger, string message, Exception exception)
        {
            logger.WriteLog(LogType.Debug, message, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Debug"/> statement with exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="exception"></param>
        public static void WriteDebug(this ILogger logger, Exception exception)
        {
            logger.WriteLog(LogType.Debug, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Debug"/> statement with message
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        public static void WriteDebug(this ILogger logger, string message)
        {
            logger.WriteLog(LogType.Debug, message);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Error"/> statement with message and exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void WriteError(this ILogger logger, string message, Exception exception)
        {
            logger.WriteLog(LogType.Error, message, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Error"/> statement with exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="exception"></param>
        public static void WriteError(this ILogger logger, Exception exception)
        {
            logger.WriteLog(LogType.Error, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Error"/> statement with message
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        public static void WriteError(this ILogger logger, string message)
        {
            logger.WriteLog(LogType.Error, message);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Fatal"/> statement with message and exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void WriteFatal(this ILogger logger, string message, Exception exception)
        {
            logger.WriteLog(LogType.Fatal, message, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Fatal"/> statement with exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="exception"></param>
        public static void WriteFatal(this ILogger logger, Exception exception)
        {
            logger.WriteLog(LogType.Fatal, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Fatal"/> statement with message
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        public static void WriteFatal(this ILogger logger, string message)
        {
            logger.WriteLog(LogType.Fatal, message);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Info"/> statement with message and exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void WriteInfo(this ILogger logger, string message, Exception exception)
        {
            logger.WriteLog(LogType.Info, message, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Info"/> statement with exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="exception"></param>
        public static void WriteInfo(this ILogger logger, Exception exception)
        {
            logger.WriteLog(LogType.Info, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Info"/> statement with message
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        public static void WriteInfo(this ILogger logger, string message)
        {
            logger.WriteLog(LogType.Info, message);
        }
        
        /// <summary>
        /// Extension to write the <see cref="LogType.Trace"/> statement with message and exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void WriteTrace(this ILogger logger, string message, Exception exception)
        {
            logger.WriteLog(LogType.Trace, message, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Trace"/> statement with exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="exception"></param>
        public static void WriteTrace(this ILogger logger, Exception exception)
        {
            logger.WriteLog(LogType.Trace, exception);
        }
        
        /// <summary>
        /// Extension to write the <see cref="LogType.Trace"/> statement with message
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        public static void WriteTrace(this ILogger logger, string message)
        {
            logger.WriteLog(LogType.Trace, message);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Warn"/> statement with message and exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void WriteWarn(this ILogger logger, string message, Exception exception)
        {
            logger.WriteLog(LogType.Warn, message, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Warn"/> statement with exception
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="exception"></param>
        public static void WriteWarn(this ILogger logger, Exception exception)
        {
            logger.WriteLog(LogType.Warn, exception);
        }

        /// <summary>
        /// Extension to write the <see cref="LogType.Warn"/> statement with message
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="message"></param>
        public static void WriteWarn(this ILogger logger, string message)
        {
            logger.WriteLog(LogType.Warn, message);
        }
    }
}