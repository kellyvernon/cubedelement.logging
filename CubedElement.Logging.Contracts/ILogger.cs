﻿using System;

namespace CubedElement.Logging.Contracts
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// 
        /// </summary>
        void Trace(string message);

        /// <summary>
        /// 
        /// </summary>
        void Trace(Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Trace(string message, Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Info(string message);

        /// <summary>
        /// 
        /// </summary>
        void Info(Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Info(string message, Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Warn(string message);

        /// <summary>
        /// 
        /// </summary>
        void Warn(Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Warn(string message, Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Debug(string message);

        /// <summary>
        /// 
        /// </summary>
        void Debug(Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Debug(string message, Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Error(string message);

        /// <summary>
        /// 
        /// </summary>
        void Error(Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Error(string message, Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Fatal(string message);

        /// <summary>
        /// 
        /// </summary>
        void Fatal(Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Fatal(string message, Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Log(LogType logType, string message);

        /// <summary>
        /// 
        /// </summary>
        void Log(LogType logType, Exception exception);

        /// <summary>
        /// 
        /// </summary>
        void Log(LogType logType, string message, Exception exception);
    }
}