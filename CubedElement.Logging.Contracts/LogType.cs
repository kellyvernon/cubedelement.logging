﻿namespace CubedElement.Logging.Contracts
{
    /// <summary>
    /// 
    /// </summary>
    public enum LogType
    {
        /// <summary>
        /// 
        /// </summary>
        Info,

        /// <summary>
        /// 
        /// </summary>
        Trace,
        
        /// <summary>
        /// 
        /// </summary>
        Debug,
        
        /// <summary>
        /// 
        /// </summary>
        Warn,
        
        /// <summary>
        /// 
        /// </summary>
        Error,
        
        /// <summary>
        /// 
        /// </summary>
        Fatal
    }
}